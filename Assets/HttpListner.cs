﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HttpListner : MonoBehaviour {

    WebServer webServer;

	// Use this for initialization
	void Start () {
        webServer = new WebServer(4444);
        webServer.HandleRequest += WebServer_HandleRequest;
        webServer.Start();
	}

    private void WebServer_HandleRequest(Request request, Response response)
    {
        response.statusCode = 200;
        response.message = "Value is here";
        response.Write("Value is: " + request.uri.ToString());
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            webServer.StopListening();
        }
	}

    private void OnApplicationQuit()
    {
        webServer.Dispose();
    }


}
