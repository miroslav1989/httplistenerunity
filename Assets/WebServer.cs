﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

public class WebServer : IDisposable
{
    public readonly int port = 8079;
    private volatile bool _acceptConnections = true;
    private TcpListener listener;

    public event System.Action<Request, Response> HandleRequest;

    private Thread _workerThread;

    public void Start()
    {
        listener = new TcpListener(System.Net.IPAddress.Any, port);
        listener.Start(8);
        _workerThread = new Thread(AcceptConnections);
        _workerThread.Start();
    }

    public WebServer(int port)
    {
        this.port = port;
    }

    public void Dispose()
    {
        if (listener != null)
            listener.Stop();
        listener = null;

        _acceptConnections = false;
       // _workerThread.Join(500);
    }

    void AcceptConnections()
    {
        try
        {
            while (_acceptConnections)
            {
                var tc = listener.AcceptTcpClient();
                ServeHTTP(tc);
            }
        }
        catch (SocketException sex)
        {
            Debug.LogError("Blocking socket call was canceled");
        }

        Debug.LogError("Thread gracefully ended!");
    }

    string ReadLine(NetworkStream stream)
    {
        var s = new List<byte>();
        while (true)
        {
            var b = (byte)stream.ReadByte();
            if (b < 0) break;
            if (b == '\n')
            {
                break;
            }
            s.Add(b);
        }
        return System.Text.Encoding.UTF8.GetString(s.ToArray()).Trim();
    }

    void ServeHTTP(TcpClient tc)
    {
        try
        {
            var stream = tc.GetStream();

            if (stream == null)
            {
                return;
            }

            var line = ReadLine(stream);

            if (line == null)
                return;
            var top = line.Trim().Split(' ');
            if (top.Length != 3)
                return;

            var req = new Request() { method = top[0], path = top[1], protocol = top[2], tcpClient = tc };
            if (req.path.StartsWith("http://"))
                req.uri = new Uri(req.path);
            else
                req.uri = new Uri("http://" + System.Net.IPAddress.Any + ":" + port + req.path);

            while (true)
            {
                var headerline = ReadLine(stream);
                if (headerline.Length == 0) break;
                req.headers.AddHeaderLine(headerline);
            }


            req.stream = stream;
            if (req.headers.Contains("Content-Length"))
            {
                var count = int.Parse(req.headers.Get("Content-Length"));
                var bytes = new byte[count];
                var offset = 0;
                while (count > 0)
                {
                    offset = stream.Read(bytes, offset, count);
                    count -= offset;
                }
                req.body = System.Text.Encoding.UTF8.GetString(bytes);
            }

            if (req.headers.Get("Content-Type").Contains("multipart/form-data"))
            {
                req.formData = MultiPartEntry.Parse(req);
            }


            var response = new Response();
            ProcessRequest(req, response);
        }
        catch (Exception e)
        {
            Debug.Log(e.StackTrace);
        }
    }

    void ProcessRequest(Request request, Response response)
    {
        if (HandleRequest != null)
        {
            HandleRequest(request, response);
        }
        request.Write(response);
        request.Close();

        Debug.Log(response.statusCode + " " + request.path);
    }

    public void StopListening()
    {
        _acceptConnections = false;
    }
}
